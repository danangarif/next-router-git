import Link from 'next/link';
// import { useEffect, useState } from 'react';


export default function List({ ownersList }) {
    // const [owners, setOwners] = useState([]);
    // useEffect(() => {
    //     async function loadData() {
    //         const response = await fetch('https://api-dev.shoesmart.co.id/products?product_ids=[4745,4900,4358,4650,4475,4331,4935,4937,4925,4681,4005,4451]&_sort=name&_order=asc&_start=0&_end=15');
    //         const ownersList = await response.json();
    //         setOwners(ownersList);
    //     }

    //     loadData();
    // }, []);

    return (
        <div>
            <h1>Product list</h1>
            {
                ownersList.map((e, index) => (
                    <div key={index}>
                        <Link as={`/product/${e.id}`} href="/[datapage]/[product]">
                            <a>{e.name}</a>
                        </Link>
                    </div>
                ))}
        </div>
    );
}

/*
* SEO CONTENT PAGE https://www.youtube.com/watch?v=Os3JZc2CtwY
*/

List.getInitialProps = async () => {
    const response = await fetch('https://api-dev.shoesmart.co.id/products?product_ids=[4745,4900,4358,4650,4475,4331,4935,4937,4925,4681,4005,4451]&_sort=name&_order=asc&_start=0&_end=15');
    const ownersList = await response.json();
    return { ownersList: ownersList }
}
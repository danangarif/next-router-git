import { useRouter } from 'next/router';
import Link from 'next/link';
// import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Product from '../../components/Product';
import { useEffect, useState } from 'react';

export default function List({ ownersList }) {
    const [brands, setBrands] = useState([]);
    const [category, setCategory] = useState([]);
    useEffect(() => {
        async function loadData() {
            const responseBrand = await fetch('https://api-dev.shoesmart.co.id/brands?_sort=id&_order=desc&_start=0&_end=10');
            const responseCategory = await fetch('https://api-dev.shoesmart.co.id/categories?_sort=category_id&_order=asc&_start=0&_end=10');
            const brandList = await responseBrand.json();
            const categoryList = await responseCategory.json();
            setBrands(brandList);
            setCategory(categoryList);
        }
        loadData();
    }, []);

    return (
        <div>
            <Container>
                <Row>


                    <Col md={4}>
                        <h1>Filter Product</h1>
                        <h3>Gender</h3>
                        <Link as={`/product`} href={`/product`}>
                            <a><div>All</div></a>
                        </Link>
                        <Link as={`/product?gender=male`} href={`/product`}>
                            <a><div>Pria</div></a>
                        </Link>
                        <Link as={`/product?gender=female`} href={`/product`}>
                            <a><div>Wanita</div></a>
                        </Link>

                        <h3>Brand</h3>
                        {
                            brands.map((e, index) => (
                                <div>{e.name}</div>
                            ))
                        }

                        <h3>Category</h3>
                        {
                            category.map((e, index) => (
                                <div>{e.name}</div>
                            ))
                        }

                    </Col>
                    <Col md={8}>
                        <h1>Product list</h1>
                        <Row>
                            {
                                ownersList.map((e, index) => (
                                    <Product key={index} data={e} />
                                ))
                            }
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

/*
* SEO CONTENT PAGE https://www.youtube.com/watch?v=Os3JZc2CtwY
*/

List.getInitialProps = async (ctx) => {
    const { query } = ctx;
    const response = await fetch('https://api-dev.shoesmart.co.id/products?product_ids=[4745,4900,4358,4650,4475,4331,4935,4937,4925,4681,4005,4451]&_sort=name&_order=asc&_start=0&_end=15&gender=' + query.gender);
    const ownersList = await response.json();
    return {
        ownersList: ownersList
    }
}
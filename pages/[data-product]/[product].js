import { useRouter } from 'next/router';

export default function Product({ ownersList }) {
    const router = useRouter();
    console.log(router.query);
    // return <h1>product : {router.query.product}</h1>
    // return <pre> {JSON.stringify(ownersList, null, 4)}</pre>
    // return <pre> {JSON.stringify(ownersList.name)}</pre>
    return (
        <div>
            <h2>{ownersList.name}</h2>
            <img src={ownersList.image_url.replace("-dev", "")} alt="" width="200px" />
            <h2>{ownersList.price}</h2>
            <div dangerouslySetInnerHTML={{ __html: ownersList.description }} />
        </div>
    );
}

/*
* SEO CONTENT PAGE https://www.youtube.com/watch?v=Os3JZc2CtwY
*/

Product.getInitialProps = async (ctx) => {
    const { query } = ctx;
    const response = await fetch('https://api-dev.shoesmart.co.id/products/' + query.id);
    const ownersList = await response.json();
    return { ownersList: ownersList }
}
import Link from 'next/link';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
const Product = (props) => {

    return (
        <Col md={4} className="mt-4">

            <div key={props.key}>
                <Link as={`/${props.data.brand_name.replace(/\s/g, "-")}/${props.data.name.replace(/\s/g, "-")}?id=${props.data.id}`} href={`/${props.data.brand_name}/[product]?id=${props.data.id}`}>
                    <a>
                        <Card>
                            <Card.Img variant="top" src={props.data.image_url.replace("-dev", "")} />
                            <Card.Body>
                                {/* <Card.Title>{props.data.name}</Card.Title> */}
                                <div>{props.data.name}</div>
                                <div className="text-dark">{props.data.price}</div>
                                {/* <Card.Text>
                                    This is a longer card with supporting text below as a natural lead-in to
                                    additional content. This content is a little bit longer.
                                </Card.Text> */}
                            </Card.Body>
                        </Card>
                    </a>
                </Link>
            </div>
        </Col>
    )

};
export default Product;
